# frozen_string_literal: true

# config.ru

require './app.rb'
run Sinatra::Application
