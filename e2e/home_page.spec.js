describe('Home page', function() {
  it('should go to the homepage', function() {
    browser.get('http://vickybrite-staging.herokuapp.com/');
    expect(browser.getTitle()).toEqual('ScotlandPHP | Home');
  });
});