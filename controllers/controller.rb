# frozen_string_literal: true

require 'sinatra'
# require('sinatra/contrib/all') if development?
require_relative('../models/category')
require_relative('../models/speaker')
require_relative('../models/talk')

get '/admin' do
  erb :"admin/dashboard", layout: :"admin/layout"
end

# Show speakers
get '/admin/speakers/?' do
  @speakers = Speaker.all
  erb :"admin/speakers/show", layout: :"admin/layout"
end

# Get new speaker form
get '/admin/speakers/new' do
  erb :"admin/speakers/new", layout: :"admin/layout"
end

# Post completed speaker form
post '/admin/speakers/new' do
  speaker = Speaker.new(params)
  speaker.save
  flash[:notice] = "You have successfully added #{speaker.full_name}!"
  redirect to '/admin/speakers'
end

# Get individual speaker to edit
get '/admin/speakers/:id/edit/?' do
  @speaker = Speaker.find(params[:id])
  erb :"admin/speakers/edit", layout: :"admin/layout"
end

# Post edited speaker form
post '/admin/speakers/:id' do
  speaker = Speaker.new(params)
  speaker.update
  flash[:notice] = "You have successfully updated #{speaker.full_name}!"
  redirect to '/admin/speakers'
end

# Delete speaker
post '/admin/speakers/:id/delete' do
  speaker = Speaker.find(params[:id])
  Speaker.delete(params[:id])
  flash[:notice] = "You have successfully deleted #{speaker.full_name}!"
  redirect to '/admin/speakers'
end

# Show talks
get '/admin/talks/?' do
  @talks = Talk.all
  erb :"admin/talks/show", layout: :"admin/layout"
end

# Get new talk form
get '/admin/talks/new' do
  @categories = Category.all
  erb :"admin/talks/new", layout: :"admin/layout"
end

# Post completed talk form
post '/admin/talks/new' do
  talk = Talk.new(params)
  talk.save
  flash[:notice] = "You have successfully added #{talk.title}!"
  redirect to '/admin/talks'
end

# Get individual talk to edit
get '/admin/talks/:id/edit/?' do
  @talk = Talk.find(params[:id])
  @categories = Category.all
  erb :"admin/talks/edit", layout: :"admin/layout"
end

# Post edited talk form
post '/admin/talks/:id' do
  talk = Talk.new(params)
  talk.update
  flash[:notice] = "You have successfully updated #{talk.title}!"
  redirect to '/admin/talks'
end

# Delete talk
post '/admin/talks/:id/delete' do
  talk = Talk.find(params[:id])
  Talk.delete(params[:id])
  flash[:notice] = "You have successfully deleted #{talk.title}!"
  redirect to '/admin/talks'
end

# Show categories
get '/admin/categories/?' do
  @categories = Category.all
  erb :"admin/categories/show", layout: :"admin/layout"
end

# Get new category form
get '/admin/categories/new' do
  erb :"admin/categories/new", layout: :"admin/layout"
end

# Post completed category form
post '/admin/categories/new' do
  category = Category.new(params)
  category.save
  flash[:notice] = "You have successfully added #{category.title}!"
  redirect to '/admin/categories'
end

# Get individual category to edit
get '/admin/categories/:id/edit/?' do
  @category = Category.find(params[:id])
  erb :"admin/categories/edit", layout: :"admin/layout"
end

# Post edited category form
post '/admin/categories/:id' do
  category = Category.new(params)
  category.update
  flash[:notice] = "You have successfully updated #{category.title}!"
  redirect to '/admin/categories'
end

# Delete category
post '/admin/categories/:id/delete' do
  category = Category.find(params[:id])
  Category.delete(params[:id])
  flash[:notice] = "You have successfully deleted #{category.title}!"
  redirect to '/admin/categories'
end
