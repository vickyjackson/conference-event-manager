# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'sinatra'
require 'sinatra/flash'
enable :sessions
# require('sinatra/contrib/all') if development?
require_relative('./models/category')
require_relative('./models/speaker')
require_relative('./models/talk')
require_relative('./controllers/controller')

# Index
get '/' do
  @speakers = Speaker.all
  erb :index
end
