# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require('minitest/autorun')
require_relative('../models/category')

# Testing the Category model
class TestCategory < Minitest::Test
  def setup
    @category1 = Category.new(
      'title' => 'CI/CD',
      'description' => 'Continuous integration or continuous deployment'
    )
  end

  def test_category_name
    assert_equal('CI/CD', @category1.title)
  end
end
