# frozen_string_literal: true

require_relative('../db/sql_runner.rb')
require_relative('../models/category.rb')
require_relative('../models/speaker.rb')
require_relative('../models/talk.rb')
require_relative('../models/speaker_talk.rb')

speaker1 = Speaker.new(
  'first_name' => 'John', 'last_name' => 'Smith',
  'job_title' => 'Developer',
  'organisation' => 'Oracle',
  'bio' => 'Lorem ipsum dolar sit amet',
  'email' => 'john_smith@oracle.net',
  'twitter' => 'john_smith',
  'linkedin' => 'john_smith',
  'website' => 'johnsmith.com',
  'image_url' => 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&h=350'
)
speaker1.save

speaker2 = Speaker.new(
  'first_name' => 'Joe',
  'last_name' => 'Bloggs',
  'job_title' => 'UX Designer',
  'organisation' => 'Adobe',
  'bio' => 'Lorem ipsum dolar sit amet',
  'email' => 'joe_bloggs@adobe.net',
  'twitter' => 'joe_bloggs',
  'linkedin' => 'joe_bloggs',
  'website' => 'joebloggs.com',
  'image_url' => 'https://images.pexels.com/photos/762020/pexels-photo-762020.jpeg?auto=compress&cs=tinysrgb&h=350'
)
speaker2.save

speaker3 = Speaker.new(
  'first_name' => 'Jenny',
  'last_name' => 'Bloggs',
  'job_title' => 'UX Designer',
  'organisation' => 'Adobe',
  'bio' => 'Lorem ipsum dolar sit amet',
  'email' => 'joe_bloggs@adobe.net',
  'twitter' => 'joe_bloggs',
  'linkedin' => 'joe_bloggs',
  'website' => 'joebloggs.com',
  'image_url' => 'https://images.pexels.com/photos/762020/pexels-photo-762020.jpeg?auto=compress&cs=tinysrgb&h=350'
)
speaker3.save

speaker4 = Speaker.new(
  'first_name' => 'Margaret',
  'last_name' => 'Bloggs',
  'job_title' => 'UX Designer',
  'organisation' => 'Adobe',
  'bio' => 'Lorem ipsum dolar sit amet',
  'email' => 'joe_bloggs@adobe.net',
  'twitter' => 'joe_bloggs',
  'linkedin' => 'joe_bloggs',
  'website' => 'joebloggs.com',
  'image_url' => 'https://images.pexels.com/photos/762020/pexels-photo-762020.jpeg?auto=compress&cs=tinysrgb&h=350'
)
speaker4.save

category1 = Category.new(
  'title' => 'CI/CD',
  'description' => 'Continuous integration or continuous deployment'
)
category1.save

category2 = Category.new(
  'title' => 'UX',
  'description' => 'User experience, user research, product lifecycle'
)
category2.save

category3 = Category.new(
  'title' => 'Ruby',
  'description' => 'User experience, user research, product lifecycle'
)
category3.save

talk1 = Talk.new(
  'title' => 'CI Uncovered',
  'abstract' => 'A talk about the fundamentals of CI',
  'category_id' => category1.id
)
talk1.save

talk2 = Talk.new(
  'title' => 'UX for Developers',
  'abstract' => 'A talk about UX aimed at developers',
  'category_id' => category2.id
)
talk2.save

talk3 = Talk.new(
  'title' => 'CI Uncovered',
  'abstract' => 'A talk about the fundamentals of CI',
  'category_id' => category1.id
)
talk3.save

talk4 = Talk.new(
  'title' => 'UX for Developers',
  'abstract' => 'A talk about UX aimed at developers',
  'category_id' => category2.id
)
talk4.save

speakertalk1 = SpeakerTalk.new(
  'speaker_id' => speaker1.id,
  'talk_id' => talk1.id
)
speakertalk1.save

speakertalk2 = SpeakerTalk.new(
  'speaker_id' => speaker2.id,
  'talk_id' => talk2.id
)
speakertalk2.save

speakertalk3 = SpeakerTalk.new(
  'speaker_id' => speaker3.id,
  'talk_id' => talk3.id
)
speakertalk3.save

speakertalk4 = SpeakerTalk.new(
  'speaker_id' => speaker4.id,
  'talk_id' => talk4.id
)
speakertalk4.save

speaker1.job_title = 'Senior Developer'
speaker1.update

category1.title = 'Continuous Integration'
category1.update

talk1.title = 'CI for Beginners'
talk1.update

# speaker1.delete()
# category1.delete()
# talk1.delete()

# p Speaker.find(3)
# p Talk.find(3)
# p Category.find(3)

p Talk.all
p Category.all
p Speaker.all
