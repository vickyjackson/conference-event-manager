# frozen_string_literal: true

require('pg')

# A class to establish connection to a database
class SqlRunner
  def self.run(sql, values = [])
    db_settings = URI.parse(ENV['DATABASE_URL'])
    db = PG.connect(
      dbname: db_settings.path[1..-1],
      host: db_settings.host,
      port: 5432, user: db_settings.user,
      password: db_settings.password
    )
    db.prepare('query', sql)
    db.exec_prepared('query', values)
  ensure db.close unless db.nil? == true
  end
end
