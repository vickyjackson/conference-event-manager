-- DROP TABLES

DROP TABLE speakers_talks;
DROP TABLE talks;
DROP TABLE speakers;
DROP TABLE categories;

-- CREATE TABLES

CREATE TABLE speakers (
    id SERIAL4 PRIMARY KEY,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    job_title VARCHAR(255),
    organisation VARCHAR(255),
    bio VARCHAR(1024),
    email VARCHAR(255),
    twitter VARCHAR(255),
    linkedin VARCHAR(255),
    website VARCHAR(255),
    image_url VARCHAR(255)
);

CREATE TABLE categories (
    id SERIAL4 PRIMARY KEY,
    title VARCHAR(255),
    description VARCHAR(255)
);

CREATE TABLE talks (
    id SERIAL4 PRIMARY KEY,
    title VARCHAR(255),
    abstract VARCHAR(255),
    category_id INT4 REFERENCES categories(id) ON DELETE SET NULL 
);

CREATE TABLE speakers_talks (
    id SERIAL4 PRIMARY KEY,
    speaker_id INT4 REFERENCES speakers(id) ON DELETE CASCADE,
    talk_id INT4 REFERENCES talks(id) ON DELETE CASCADE
);
