# frozen_string_literal: true

require_relative('../db/sql_runner.rb')

# A class for creating, updating and deleting speakers_talks
class SpeakerTalk
  def initialize(options)
    @id = options['id'].to_i unless options['id'].nil?
    @speaker_id = options['speaker_id']
    @talk_id = options['talk_id']
  end

  # Create
  def save
    sql = "INSERT INTO speakers_talks (speaker_id, talk_id)
    VALUES ($1, $2) RETURNING id"
    values = [@speaker_id, @talk_id]
    result = SqlRunner.run(sql, values)
    @id = result[0]['id']
  end

  # Read
end
