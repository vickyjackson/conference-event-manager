# frozen_string_literal: true

require_relative('../db/sql_runner.rb')

# A class for creating, updating and deleting talks
class Talk
  attr_reader :id
  attr_accessor :title, :abstract, :category_id

  # Pass in a hash for more flexibility
  def initialize(options)
    @id = options['id'].to_i unless options['id'].nil?
    @title = options['title']
    @abstract = options['abstract']
    @category_id = options['category_id']
  end

  # Create
  def save
    sql = "INSERT INTO talks (title, abstract, category_id)
    VALUES ($1, $2, $3) RETURNING id"
    values = [@title, @abstract, @category_id]
    result = SqlRunner.run(sql, values)
    @id = result[0]['id']
  end

  # Read
  def self.all
    sql = 'SELECT * from talks ORDER BY title, abstract ASC'
    result = SqlRunner.run(sql)
    talks = result.map { |talk| Talk.new(talk) }
    talks
  end

  def self.find(id)
    sql = 'SELECT * FROM talks WHERE id = $1'
    values = [id]
    result = SqlRunner.run(sql, values)
    talk = Talk.new(result[0])
    talk
  end

  def speakers
    sql = 'SELECT speakers.* FROM speakers INNER JOIN speakers_talks ON
    speakers_talks.speaker_id = speaker.id WHERE speakers_talks.talk_id = $1;'
    values = [@id]
    result = SqlRunner.run(sql, values)
    speakers = result.map { |speaker| Speaker.new(speaker) }
    speakers
  end

  # Update
  def update
    sql = 'UPDATE talks SET (title, abstract, category_id) = ($1, $2, $3)
    WHERE id = $4'
    values = [@title, @abstract, @category_id, @id]
    SqlRunner.run(sql, values)
  end

  # Delete
  def self.delete(id)
    sql = 'DELETE FROM talks WHERE id = $1'
    values = [id]
    SqlRunner.run(sql, values)
  end
end
