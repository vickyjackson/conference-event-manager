# frozen_string_literal: true

require_relative('../db/sql_runner.rb')

# A class for creating, updating and deleting speakers
class Speaker
  attr_reader :id
  attr_accessor :first_name, :last_name, :job_title, :organisation,
                :bio, :email, :twitter, :linkedin, :website, :image_url

  # pass in a hash for more flexibility
  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def initialize(options)
    @id = options['id'].to_i unless options['id'].nil?
    @first_name = options['first_name']
    @last_name = options['last_name']
    @job_title = options['job_title']
    @organisation = options['organisation']
    @bio = options['bio']
    @email = options['email']
    @twitter = options['twitter']
    @linkedin = options['linkedin']
    @website = options['website']
    @image_url = options['image_url']
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength

  # Create
  def save
    sql = "INSERT INTO speakers
    (first_name, last_name, job_title, organisation,
      bio, email, twitter, linkedin, website, image_url)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id"
    values = [@first_name, @last_name, @job_title, @organisation,
              @bio, @email, @twitter, @linkedin, @website, @image_url]
    result = SqlRunner.run(sql, values)
    @id = result[0]['id']
  end

  # Read
  def self.all
    sql = 'SELECT * from speakers ORDER BY first_name, last_name ASC'
    result = SqlRunner.run(sql)
    speakers = result.map { |speaker| Speaker.new(speaker) }
    speakers
  end

  def self.find(id)
    sql = 'SELECT * FROM speakers WHERE id = $1'
    values = [id]
    result = SqlRunner.run(sql, values)
    speaker = Speaker.new(result[0])
    speaker
  end

  def talks
    sql = 'SELECT talks.* FROM talks INNER JOIN speakers_talks ON
    speakers_talks.talk_id = talks.id WHERE speakers_talks.speaker_id = $1;'
    values = [@id]
    result = SqlRunner.run(sql, values)
    talks = result.map { |talk| Talk.new(talk) }
    talks
  end

  # Update
  def update
    sql = 'UPDATE speakers SET
    (first_name, last_name, job_title, organisation, bio,
      email, twitter, linkedin, website, image_url) =
      ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
      WHERE id = $11'
    values = [@first_name, @last_name, @job_title, @organisation, @bio, @email,
              @twitter, @linkedin, @website, @image_url, @id]
    SqlRunner.run(sql, values)
  end

  # Delete
  def self.delete(id)
    sql = 'DELETE FROM speakers WHERE id = $1'
    values = [id]
    SqlRunner.run(sql, values)
  end

  # Other methods
  def full_name
    "#{@first_name} #{@last_name}"
  end

  def full_job
    "#{@job_title} at #{@organisation}"
  end
end
