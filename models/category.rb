# frozen_string_literal: true

require_relative('../db/sql_runner.rb')

# A class for creating, updating and deleting categories
class Category
  attr_reader :id
  attr_accessor :title, :description

  # Pass in a hash for more flexibility
  def initialize(options)
    @id = options['id'].to_i unless options['id'].nil?
    @title = options['title']
    @description = options['description']
  end

  # Create
  def save
    sql = "INSERT INTO categories (title, description)
    VALUES ($1, $2) RETURNING id"
    values = [@title, @description]
    result = SqlRunner.run(sql, values)
    @id = result[0]['id']
  end

  # Read
  def self.all
    sql = 'SELECT * from categories ORDER BY title, description ASC'
    result = SqlRunner.run(sql)
    categories = result.map { |category| Category.new(category) }
    categories
  end

  def self.find(id)
    sql = 'SELECT * FROM categories WHERE id = $1'
    values = [id]
    result = SqlRunner.run(sql, values)
    category = Category.new(result[0])
    category
  end

  # Update
  def update
    sql = 'UPDATE categories SET (title, description) = ($1, $2) WHERE id = $3'
    values = [@title, @description, @id]
    SqlRunner.run(sql, values)
  end

  # Delete
  def self.delete(id)
    sql = 'DELETE FROM categories WHERE id = $1'
    values = [id]
    SqlRunner.run(sql, values)
  end
end
