exports.config = {
  framework: 'jasmine',
  specs: ['./e2e/*.spec.js'],
  chromeOnly: true,
  capabilities: {
    browserName: 'chrome',
    'chromeOptions': {
      'args': [
        '--headless', 'no-sandbox'
      ]
    }
  },
  directConnect: true,
  jasmineNodeOpts: {
    realtimeFailure: true,
    showColors: true,
    defaultTimeoutInterval: 30000,
  },
  onPrepare: function () {
    browser.ignoreSynchronization=true;
  }
}